from django import forms
from django.contrib.auth.models import User
from .models import Video,Image,Documents

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username','email','password')

class Video_form(forms.ModelForm):

    class Meta():
        model = Video
        fields = ('caption','video')


class ImageForm(forms.ModelForm):
    class Meta:
        model=Image
        fields=("captionim","image")

class DocumentsForm(forms.ModelForm):
    class Meta:
        model = Documents
        fields = ("captiondoc","docs")
